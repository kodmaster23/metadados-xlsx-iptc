import re
import os
import pandas as pd
import json
import sys
import chardet
from datetime import datetime
import shutil
from iptcinfo3 import IPTCInfo
import glob
import numpy as np
import nltk
import spacy
import locationtagger
import gettext
import pycountry



country_codes_to_names = {
    "AF": "afeganistão",
    "AL": "albânia",
    "DZ": "argélia",
    "AD": "andorra",
    "AO": "angola",
    "AG": "antígua e barbuda",
    "AR": "argentina",
    "AM": "armênia",
    "AU": "austrália",
    "AT": "austria",
    "AZ": "azerbaijão",
    "BS": "bahamas",
    "BH": "bahrein",
    "BD": "bangladesh",
    "BB": "barbados",
    "BY": "bielorrússia",
    "BE": "bélgica",
    "BZ": "belize",
    "BJ": "benim",
    "BT": "butão",
    "BO": "bolívia",
    "BA": "bosnia e hercegovina",
    "BW": "botswana",
    "BR": "brasil",
    "BN": "brunei",
    "BG": "bulgária",
    "BF": "burkinafaso",
    "BI": "burundi",
    "KH": "cambodja",
    "CM": "camarões",
    "CA": "canadá",
    "CF": "república centro-africana",
    "TD": "chade",
    "CL": "chile",
    "CN": "china",
    "CO": "colômbia",
    "KM": "comores",
    "CG": "congo (brazzaville)",
    "CD": "congo (kinshasa)",
    "CR": "costa rica",
    "CI": "costa do marfim",
    "HR": "croácia",
    "CU": "cuba",
    "CY": "chipre",
    "CZ": "republica checa",
    "DK": "dinamarca",
    "DJ": "djibouti",
    "DM": "domínica",
    "DO": "república dominicana",
    "TL": "timor-leste",
    "EC": "equador",
    "EG": "egito",
    "SV": "el salvador",
    "GQ": "guiné equatorial",
    "ER": "eritrea",
    "EE": "estônia",
    "ET": "etiópia",
    "FJ": "fiji",
    "FI": "finlândia",
    "FR": "frança",
    "GA": "gabão",
    "GM": "gâmbia",
    "GE": "geórgia",
    "DE": "alemanha",
    "GH": "gana",
    "GR": "grécia",
    "GD": "grenada",
    "GT": "guatemala",
    "GN": "guiné",
    "GW": "guiné-bissau",
    "GY": "guyana",
    "HT": "haiti",
    "HN": "hondures",
    "HU": "hungria",
    "IS": "islandia",
    "IN": "índia",
    "ID": "indonésia",
    "IR": "iran",
    "IQ": "iraque",
    "IE": "irlanda",
    "IT": "italia",
    "JM": "jamaica",
    "JP": "japão",
    "JO": "jordânia",
    "KZ": "cazaquistão",
    "KE": "quénia",
    "KI": "kiribati",
    "KP": "coreia do norte",
    "KR": "coreia do sul",
    "XK": "kosovo",
    "KW": "kuwait",
    "KG": "quistão",
    "LA": "laos",
    "LV": "letônia",
    "LB": "libano",
    "LS": "lesoto",
    "LR": "libéria",
    "LY": "líbia",
    "LI": "liechtenstein",
    "LT": "lituânia",
    "LU": "luxemburgo",
    "MK": "macedônia",
    "MG": "madagascar",
    "MW": "malawi",
    "MY": "malásia",
    "MV": "maldivas",
    "ML": "mali",
    "MT": "malta",
    "MH": "ilhas marshall",
    "MR": "mauritânia",
    "MU": "maurício",
    "MX": "mexico",
    "FM": "micronésia",
    "MD": "moldevia",
    "MC": "mônaco",
    "MN": "mongólia",
    "ME": "montenegro",
    "MA": "marrocos",
    "MZ": "mozambique",
    "MM": "mianmar (birmania)",
    "NA": "namíbia",
    "NR": "nauru",
    "NP": "nepal",
    "NL": "holanda",
    "NZ": "nova zelândia",
    "NI": "nicarágua",
    "NG": "nigeria",
    "NO": "noruega",
    "OM": "omã",
    "PK": "paquistão",
    "PW": "palau",
    "PA": "panamá",
    "PG": "papua nova guiné",
    "PY": "paraguai",
    "PE": "peru",
    "PH": "filipinas",
    "PL": "polônia",
    "PT": "portugal",
    "QA": "catar",
    "RO": "romênia",
    "RU": "rússia",
    "RW": "ruanda",
    "LC": "são cristóvão e nêvis",
    "VC": "santa lúcia",
    "WS": "samoa",
    "SM": "são marino",
    "ST": "são tomé e príncipe",
    "SA": "arábia saudita",
    "SN": "senegal",
    "RS": "sérvia",
    "SC": "seicheles",
    "SL": "serra leoa",
    "SG": "singapura",
    "SK": "eslováquia",
    "SI": "eslovênia",
    "SB": "ilhas salomão",
    "SO": "somália",
    "ZA": "áfrica do sul",
    "SS": "sudão do sul",
    "ES": "espanha",
    "LK": "sri lanka",
    "SD": "sudão",
    "SR": "suriname",
    "SZ": "suazilândia",
    "SE": "suécia",
    "CH": "suíça",
    "SY": "síria",
    "TJ": "tajiquistão",
    "TZ": "tanzânia",
    "TH": "tailândia",
    "TG": "togo",
    "TO": "tonga",
    "TT": "trinidad e tobago",
    "TN": "tunísia",
    "TR": "turquia",
    "TM": "turcomenistão",
    "TV": "tuvalu",
    "UG": "uganda",
    "UA": "ucrânia",
    "AE": "emirados árabes unidos",
    "GB": "reino unido",
    "US": "estados unidos",
    "UY": "uruguai",
    "UZ": "uzbequistão",
    "VU": "vanuatu",
    "VN": "vietnã",
    "YE": "iêmen",
    "ZM": "zambia",
    "ZW": "zimbábue",
}
brazil_states = [
    {"uf": "AC", "fullname": "Acre"},
    {"uf": "AL", "fullname": "Alagoas"},
    {"uf": "AM", "fullname": "Amazonas"},
    {"uf": "AP", "fullname": "Amapá"},
    {"uf": "BA", "fullname": "Bahia"},
    {"uf": "CE", "fullname": "Ceará"},
    {"uf": "DF", "fullname": "Distrito Federal"},
    {"uf": "ES", "fullname": "Espírito Santo"},
    {"uf": "GO", "fullname": "Goiás"},
    {"uf": "MA", "fullname": "Maranhão"},
    {"uf": "MG", "fullname": "Minas Gerais"},
    {"uf": "MS", "fullname": "Mato Grosso do Sul"},
    {"uf": "MT", "fullname": "Mato Grosso"},
    {"uf": "PA", "fullname": "Pará"},
    {"uf": "PB", "fullname": "Paraíba"},
    {"uf": "PE", "fullname": "Pernambuco"},
    {"uf": "PI", "fullname": "Piauí"},
    {"uf": "PR", "fullname": "Paraná"},
    {"uf": "RJ", "fullname": "Rio de Janeiro"},
    {"uf": "RN", "fullname": "Rio Grande do Norte"},
    {"uf": "RO", "fullname": "Rondônia"},
    {"uf": "RR", "fullname": "Roraima"},
    {"uf": "RS", "fullname": "Rio Grande do Sul"},
    {"uf": "SC", "fullname": "Santa Catarina"},
    {"uf": "SE", "fullname": "Sergipe"},
    {"uf": "SP", "fullname": "São Paulo"},
    {"uf": "TO", "fullname": "Tocantins"}
]
uf_to_fullname = {file["uf"]: file["fullname"] for file in brazil_states}


nltk.downloader.download('maxent_ne_chunker',quiet=True)
nltk.downloader.download('words',quiet=True)
nltk.downloader.download('treebank',quiet=True)
nltk.downloader.download('maxent_treebank_pos_tagger',quiet=True)
nltk.downloader.download('punkt',quiet=True)
nltk.downloader.download('averaged_perceptron_tagger',quiet=True)

pt_br = gettext.translation('iso3166-1', pycountry.LOCALES_DIR, languages=['pt_BR'])
pt_br.install()

def convert_to_snake_case(string):
    string = str(string)
    return re.sub('[ -]', '_', string.lower())

def extract_metadata_from_excel(excel_file_path):
    excel_files = glob.glob(os.path.join(excel_file_path,"*.xlsx"))
    try:
        excel_file_path = excel_files[0]
    except:
        print("No excel files found in the folder.")
        sys.exit()

    df = pd.read_excel(excel_file_path)
    df['item_name'] = df['Item'].apply(convert_to_snake_case)

    metadata = {}
    for index, row in df.iterrows():
        line = row['Âmbito e Conteúdo']
        if not isinstance(line, str):
            continue       
        encoding = chardet.detect(line.encode())['encoding']
        line = line.encode(encoding).decode('utf-8')
        if line and isinstance(line, str):
            parts = line.split('|')
            item_name = df.loc[index, 'item_name']
            
            if not isinstance(item_name, str) or not item_name.strip() or item_name == 'nan':
                continue
            
            metadata[item_name] = extract_item_metadata(parts, df, index)
            metadata[item_name]['objeto'] = df.loc[index, 'Item']
            metadata[item_name]['titulo'] = df.loc[index, 'Título']
            metadata[item_name]['descricao'] = extract_description(df, index, metadata[item_name])
            metadata[item_name]['local'] = extract_local(df, index, metadata[item_name])
            metadata[item_name]['data_tirada'] = extract_date(df, index, 'Data (tirada)', metadata[item_name])
            metadata[item_name]['palavras_chave'] = extract_keywords(df, index, metadata[item_name])

    return metadata

def extract_item_metadata(parts, df, index):
    item_metadata = {}
    for part in parts:
        sub_parts = part.split('-')
        if len(sub_parts) > 1:
            key = convert_to_snake_case(sub_parts[0].strip())
            value = extract_value(key, sub_parts)
            if not item_metadata.get(key):
                item_metadata[key] = value
    return item_metadata

def extract_value(key, sub_parts):
    if key == 'tamanho_filme':
        return sub_parts[1].strip().replace(' ', '')
    elif key == 'brasil':
        return extract_brasil_value(sub_parts)
    else:
        return sub_parts[1].strip()

def extract_brasil_value(sub_parts):
    return {
        "country": sub_parts[0].strip() if sub_parts[0] else None,
        "region": sub_parts[1].strip() if sub_parts[1] else None,
        "city": sub_parts[2].strip() if sub_parts[2] else None
    }  
def extract_local(df, index, metadata):

    print(metadata.get('descricao'))
    places = locationtagger.find_locations(text = metadata.get('descricao'))

    city = places.cities[0] if places.cities else None
    region = places.regions[0] if places.regions else list(places.region_cities.keys())[0] if places.region_cities else None
    country = places.countries[0] if places.countries else list(places.country_cities.keys())[0] if places.country_cities else None

    if country:
        return {
                "city": city,
                "region": region.replace("Estado de ", "") if region and "Estado de" in region else region,
                "country":_(country),
            }

    match = re.search(r'^([^(]+)\(([^)]+)\)$', df.loc[index, 'Local'])
    if match:
        print(match.group(1).strip())
        places = locationtagger.find_locations(text=match.group(1).strip())
        city =  places.cities[0] if places.cities else None

        places = locationtagger.find_locations(text=match.group(2).strip())
        region = places.regions[0] if places.regions else None
        country = places.countries[0] if places.countries else list(places.country_cities.keys())[0] if places.country_cities else None
        
        print(city, region, country)
        return {
                "city": city,
                "region": region,
                "country": _(country),
            }


    places = locationtagger.find_locations(text = df.loc[index, 'Local'])
    
    city = places.cities[0] if places.cities else None
    region = places.regions[0] if places.regions else list(places.region_cities.keys())[0] if places.region_cities else None
    country = places.countries[0] if places.countries else list(places.country_cities.keys())[0] if places.country_cities else None
    
    if country:
        return {
                "city": city,
                "region": region.replace("Estado de ", "") if region and "Estado de" in region else region,
                "country":_(country),
            }
    
    if metadata.get('brasil'):
        return metadata.get('brasil')
       
    
def extract_date(df, index, column_name, metadata):
    date_string = df.loc[index, column_name]
    
    if isinstance(date_string, np.float64):
        date_string = str(int(date_string))
        try:
            date_object = datetime.strptime(date_string, '%Y')
            return date_object.strftime('%Y')
        except ValueError:
            pass
    
    if date_string :
        return date_string.strftime('%Y')
    else:
        date_string = metadata.get('data_tirada')
        date_formats = ['%b\\ %y', '%b %Y','%b. %Y', '%Y', '%d-%m-%Y', '%d.%m.%y']
        for date_format in date_formats:
            try:
                date_object = datetime.strptime(date_string, date_format)
                if date_object.year < 1500:
                    continue
                if date_format == '%d-%m-%Y' or date_format == '%d.%m.%y':
                    return date_object.strftime('%d/%m/%Y')
                return date_object.strftime('%Y')
            except ValueError:
                pass
    return None

def extract_description(df, index, metadata):
    if df.loc[index, 'Digitalização do negativo'] == 'Ausente':
        return  metadata['assunto'] + '. Neg. Ausente'
    else:
        negativo_ausente = df.loc[index, 'Neg. Ausente']
        notacao_negativo = df.loc[index, 'Notação de negativo']
        return metadata['assunto'] + f'. {negativo_ausente}. {notacao_negativo}.'

def extract_keywords(df, index, metadata):
    
    fundo = ""
    try:
        fundo = df.loc[index, 'Fundo'].strip()+ ";"
    except:
        pass
    return f"{fundo}{metadata['objeto']};{metadata['local']['region']};{metadata['data_tirada']};"


def write_metadata_to_file(metadata, output_file_path):
    os.makedirs(os.path.dirname(output_file_path), exist_ok=True)
    with open(output_file_path, 'w', encoding='utf-8') as f:
        json.dump(metadata, f, indent=4, ensure_ascii=False)

def write_metadata_to_obj(output_file_path, folder):
    
    # Read the output data from the JSON file
    with open(output_file_path, 'r', encoding='utf-8') as f:
        output_data = json.load(f)

    # Iterate over each file in the output data
    for file_name, metadata in output_data.items():
        # Define the path to the objects directory
        objects_dir = os.path.join(".", "data", "inputs", folder )

        # Define the path to the image file
        image_path = os.path.join(objects_dir, f"{file_name}.jpg")

        print(f"Copying {image_path} to {os.path.dirname(output_file_path)}")
        # Check if the image file exists
        if os.path.exists(image_path):
            print(f"Copying {image_path} to {os.path.dirname(output_file_path)}")
            # Copy the image file to the output directory
            shutil.copy(image_path, os.path.join(os.path.dirname(output_file_path), f"{file_name}.jpg"))

            # Define the path to the copied image file
            image_path = os.path.join(os.path.dirname(output_file_path), f"{file_name}.jpg")

            # Create IPTCInfo object for the image file
            iptc_data = IPTCInfo(image_path, force=True)

            # Iterate over each metadata item
            for key, value in metadata.items():
                # Update IPTC data based on the metadata item
                # if key == 'objeto':
                #     iptc_data['Object Name'] = value
                if key == 'titulo':
                    iptc_data['Object Name'] = value
                elif key == 'local':
                    for k, v in metadata['local'].items():
                        if k == 'city':
                            iptc_data['city'] = v if v else None
                        elif k == 'region':
                            iptc_data['province/state'] = v if v else None
                        elif k == 'country':
                            iptc_data['country/primary location name'] = v if v else None
                            if v is not None:
                                for code, name in country_codes_to_names.items():
                                    if name == v.lower():
                                        iptc_data['country/primary location code'] = code
                                        break
                        
                elif key == 'data_tirada':
                    try:
                        datetime.strptime(value, '%d/%m/%Y')
                        iptc_data['Date Created'] = value
                    except ValueError:
                        pass 
                elif key == 'descricao':
                    iptc_data['Caption/Abstract'] = value
                elif key == 'palavras_chave':
                    iptc_data['Keywords'] = value.split(';')

                iptc_data["source"] = "Fotografia com ficha"
            # Write the updated IPTC data to the image file
            iptc_data.save()
            
    # Print the path to the output file
    print(f"Metadata updated and saved in {output_file_path}")

if __name__ == "__main__":
    
    try:
        folder = sys.argv[1]
    except:
        folder = ''
        
    excel_file_path = os.path.join(".", "data", "inputs", folder)
    output_file_path = os.path.join(".", "data", "outputs",  folder, "output.json")
    metadata = extract_metadata_from_excel(excel_file_path)
    write_metadata_to_file(metadata, output_file_path)
    write_metadata_to_obj(output_file_path, folder)
    

