import sys
import json
import os
from PIL.ExifTags import TAGS
from PIL import Image
from iptcinfo3 import IPTCInfo, c_datasets

def compare_image_metadata(image1_path, image2_path):
    image1 = Image.open(image1_path)
    image2 = Image.open(image2_path)

    exif1 = {
        TAGS.get(k, k): v for k, v in image1._getexif().items()
    }
    exif2 = {
        TAGS.get(k, k): v for k, v in image2._getexif().items()
    }

    iptc1 = IPTCInfo(image1_path, force=True,inp_charset='utf-8',out_charset='latin-1')
    iptc2 = IPTCInfo(image2_path, force=True,inp_charset='utf-8',out_charset='latin-1')

    print('Comparing image metadata...')
    print(iptc2[200])
    differences = {}
    for key in c_datasets:
        if iptc1[key] != iptc2[key]:
            v1 = iptc1[key]          
            v2 = iptc2[key]
            differences[c_datasets[key]] = {
                'image1': v1,
                'image2': v2
            }

    for key, value2 in exif1.items():
        if value2 != exif2.get(key):
            differences[key] = {
                'image1': str(value2).encode('utf-8'),
                'image2': str(exif2.get(key)).encode('utf-8')
            }
    for key, value2 in exif2.items():
        if exif1.get(key) is None:
            differences[key] = {
                'image1': None,
                'image2': str(value2).encode('utf-8')
            }
    return differences

def main(image1_path, image2_path, output_path):
    differences = compare_image_metadata(image1_path, image2_path)

    # Convert bytes to strings
    differences_str = {k: {key: str(value, 'utf-8') if isinstance(value, bytes) else value for key, value in v.items()} for k, v in differences.items()}
    os.makedirs(os.path.dirname(output_path), exist_ok=True)
    with open(output_path, 'w', encoding='utf-8') as f:
        print(f"Writing differences to {output_path}")
        json.dump(differences_str, f, indent=4, ensure_ascii=False)


if __name__ == "__main__":
    image1_path = sys.argv[1]
    image2_path = sys.argv[2]
    output_path = sys.argv[3]
    main(image1_path, image2_path, output_path)


