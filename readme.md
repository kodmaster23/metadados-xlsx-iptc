# Instruções para execução

Para executar o script, siga as instruções abaixo:

1. Certifique-se de ter o Python instalado em sua máquina. Para instalar o Python, acesse https://www.python.org/downloads e siga as instruções para baixar e instalar a versão adequada para o seu sistema operacional.

2. Abra um terminal e navegue para dentro da pasta do projeto

3. Crie um ambiente virtual (venv) para evitar conflitos de dependências:

```
python -m venv env
```
   
4. Ative o ambiente virtual.
```
 env\Scripts\activate
```
5. Instale as dependências do projeto usando o comando ``.
```
pip install -r requirements.txt
```

6. Copie as imagens e o arquivo xlsx para uma pasta chamada `inputs` no diretório raiz do projeto. Exemplo de estrutura de diretórios:
```
├───data
│    └───inputs
│       └───[Nome da pasta com as imagens e xlsx]
```
7. Execute o comando passando por parâmetro o nome da pasta que você criou no inputs:
```
python add-metadata.py [Nome da pasta com as imagens e xlsx]
```
8. Verifique a pasta outputs para as imagens com os metadados:
```
├───data
│    └───outputs
│       └───[Nome da pasta com as imagens e xlsx]
```